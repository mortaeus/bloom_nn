import numpy as np
from stochastic_bloom_filter import StochasticBloomFilter
from repeating_code import StochasticRepeatingCode
import random
from noisy_channel import *
import math
import itertools
from multiprocessing import Pool
import sys
from bitarray import bitarray

num_reps = range(1, 10, 2)
num_hashes = range(36, 100, 2)       #number of hashes used
fs = [0, 0.1, 0.2, 0.3]
num_tests = 10000   #number of tests we run per configuration

random.seed(42) #does not give us exactly same results each time as we 
                #run each thread in separate process

def bit_correct(a, b) :
    return np.sum((np.array(bitarray(format(a, '#010b')[2:]).tolist()).astype(float) ==
                np.array(bitarray(format(b, '#010b')[2:]).tolist()).astype(float)).astype(float))

#calculate log probability of 
total = 0
def BF_run(params) :
  (BF_size, num_hash, f) = params
  if num_hash > BF_size / 1.5 :
    return (0, 0, 0, 0, 0, 0)
  global total
  total += 1
  print total
  total_log_prob = 0
  num_correct = 0
  num_bit_correct = 0
  for i in range(num_tests) :
    block = random.randrange(256)
    sbf = StochasticBloomFilter.create(block, BF_size, num_hash)
    noisy_sbf = bsc_noise(sbf, f)
    if noisy_sbf.dquery() == block :
      num_correct += 1
    prob = noisy_sbf.squery()[block]
    if prob > 0 :
      total_log_prob += math.log(prob)
    num_bit_correct += bit_correct(block, noisy_sbf.dquery())
  perplexity = 2**(-(1.0 / num_tests) * total_log_prob)
  results = (BF_size, num_hash, f, float(num_correct) / num_tests, perplexity, num_bit_correct / (num_tests * 8))
  return results

total = 0
def REP_run(params) :
  (num_reps, f) = params
  global total
  total += 1
  total_log_prob = 0
  num_correct = 0
  num_bit_correct = 0
  print total
  for i in range(num_tests) :
    block = random.randrange(256)
    src = StochasticRepeatingCode.create(block, num_reps)
    noisy_src = bsc_noise(src, f)
    if noisy_src.dquery() == block :
      num_correct += 1
    prob = noisy_src.squery()[block]
    num_bit_correct += bit_correct(block, noisy_src.dquery())
    if prob > 0 :
      total_log_prob += math.log(prob)
  perplexity = 2**(-(1.0 / num_tests) * total_log_prob)
  results = (num_reps, f, float(num_correct) / num_tests, perplexity, num_bit_correct / (num_tests * 8))
  return results

def parse_args() : 
  if len(sys.argv) != 3 :
    print "Use it properly"
    exit(1)
  else :
    return (sys.argv[1], sys.argv[2])

def main() :
  (filename_BF, filename_REP) = parse_args()

  #get bloom filter results
  BF_sizes = map(lambda x : x * 8, num_reps)
  params = itertools.product(BF_sizes, num_hashes, fs)
  p = Pool(40)
  results = p.map(BF_run, params)

  with open(filename_BF, 'w') as f :
    f.write('BF_size,num_hash,f,proprotion_correct,perplexity,prop_bit_correct\n')
    for r in results :
      if r[0] != 0 :
          f.write('{0},{1},{2},{3},{4},{5}\n'.format(*r))

  #get repetition code results
  params = itertools.product(num_reps, fs)
  p = Pool(40)
  results = p.map(REP_run, params)

  with open(filename_REP, 'w') as f :
    f.write('num_reps,f,proprotion_correct,perplexity,prop_bit_correct\n')
    for r in results :
      f.write('{0},{1},{2},{3},{4}\n'.format(*r))

if __name__ == '__main__' :
  main()
