from bitarray import bitarray
from bitstring import BitArray
import numpy as np

class StochasticRepeatingCode(object) :
  def tonumpy(self) :
      """Converts this bloom-filters bit array to a numpy float array,
         where 1.0 represents 1, and 0.0 represents 0"""
      return np.array(self._bit_array.tolist()).astype(float)

  def __init__(self, bit_array) :
      self._bit_array = bit_array.copy()
      
  @classmethod
  def create(cls, number, number_of_reps) :
    b = bitarray(format(number, '#010b')[2:]) * number_of_reps
    return cls(b)

  def copy(self) :
    return StochasticRepeatingCode(self._bit_array)

  def _query(self) :
    assert(len(self._bit_array) % 8 == 0)
    num_repeats = len(self._bit_array) / 8
    np_bitarr = self.tonumpy()
    per_byte = np.matrix(np.reshape(np_bitarr, (num_repeats, 8)))
    per_bit_proportions = np.squeeze(np.asarray(np.sum(per_byte, axis=0) / num_repeats))
    guessed_bits = bitarray((per_bit_proportions >= 0.5).tolist())

    return guessed_bits


  def num_distance(self, bit_array, num) :
      return sum((np.array(bitarray(format(num, '#010b')[2:]).tolist()).astype(float)
                              == np.array(bit_array.tolist()).astype(float)).astype(float) / 8)

  def squery(self) :
    """Gives distribution over all possible keys"""
    guessed_bit_array = self._query()
    unnormalised = map(lambda n : self.num_distance(guessed_bit_array, n), range(0, 256))
    if sum(unnormalised) == 0 :
      return [1.0 / 256] * 256
    else :
      return map(lambda un : un / sum(unnormalised), unnormalised)

  def dquery(self) :
    """gives best guess as to content"""
    return BitArray(self._query()).uint
