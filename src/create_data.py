import numpy as np
from stochastic_bloom_filter import *
from noisy_channel import *

m = 24
k = 11
fs = [0.1, 0.2, 0.3, 0.4]

num_points_per_block = 1000

def main() :
    random.seed(42)

    for f in fs :
        print f
        num_examples = 256 * num_points_per_block
        X = np.empty((num_examples, 24))
        y = np.empty(num_examples)

        count = 0
        for b in range(256) :
            for i in range(0, num_points_per_block) :
                sbf = StochasticBloomFilter.create(b, m, k)
                noisy_sbf = bsc_noise(sbf, f)
                X[count, :] = noisy_sbf.tonumpy()
                y[count] = b
                count += 1
        np.save('./data/in_{0}_{1}_{2}'.format(m, k, f), X)
        np.save('./data/target_{0}_{1}_{2}'.format(m, k, f), y)

if __name__ == '__main__' :
    main()
