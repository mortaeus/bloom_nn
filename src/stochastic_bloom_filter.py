#Bloom filter (BF) for single integer in the range [0, 255], i.e. a byte of information
#can use deterministic or stochastic queries. A fancy way of saying, resepctively:
# - normal boom filter: check if all bits set
# - will return the probability that BF contains key given that
#   some bits might be corrupted. Probability is proportion of bits that are
#   correct for that key. Can also return a distribution over all numbers
#hashing uses the MD5 hash function

import numpy as np
from hashlib import md5
import random
from bitarray import bitarray
import string
import copy

def _random_seed(n = 6) :
  seed = ''.join(random.choice(string.ascii_letters) for _ in range(0, n))
  return seed


class StochasticBloomFilter(object) :
  def tonumpy(self) :
      """Converts this bloom-filters bit array to a numpy float array,
         where 1.0 represents 1, and 0.0 represents 0"""
      return np.array(self._bit_array.tolist()).astype(float)

  @classmethod
  def _indices(cls, n, hash_functions, bit_array) :
    #get list of indices for provided number from each has function
    indices = []
    for h in hash_functions :
      _h = h.copy()
      _h.update(str(n))
      indices.append(int(_h.hexdigest(), 16) % len(bit_array))

    return indices

  def __init__(self, _bit_array, _hash_functions) :
    self._bit_array = _bit_array
    self._hash_functions = _hash_functions

  def _query(self, n) :
    #get proportion of bits set for query n
    indices = StochasticBloomFilter._indices(n, self._hash_functions, self._bit_array)
    number_on = sum(map(lambda i : self._bit_array[i], indices))
    return float(number_on) / len(self._hash_functions)

  def squery(self) :
    """Gives distribution over all possible keys"""
    unnormalised = map(self._query, range(0, 256))
    if sum(unnormalised) == 0 :
      return [1.0 / 256] * 256
    else :
      return map(lambda un : un / sum(unnormalised), unnormalised)
    
  def dquery(self) :
    """gives BF's best guess as to content"""
    distribution = self.squery()
    return distribution.index(max(distribution))

  def copy(self) :
    hf = map(lambda h : h.copy(), self._hash_functions)
    return StochasticBloomFilter(self._bit_array.copy(), hf)

  @classmethod
  def create(cls, number, size_in_bits, number_of__hash_functions) :
    """Puts number into the bloom filter consistent of length size_in_bits
       using specified number of hash-functions.
       @param number : the number to hash
       @param size_in_bits : length of bloom filter
       @param number_of__hash_functions : the number of hash funcitons to use
        in embedding key"""
    #create bitarray and hash
    bit_array = bitarray(size_in_bits)
    bit_array.setall(False)
    hash_functions = map(md5, [_random_seed() for x in range(number_of__hash_functions)])

    #and number to our bloom filter
    indices = cls._indices(number, hash_functions, bit_array)
    for i in indices :
      bit_array[i] = 1
  
    return cls(bit_array, hash_functions)
