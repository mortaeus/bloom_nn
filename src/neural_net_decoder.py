from sklearn.neural_network import MLPClassifier
from sklearn.cross_validation import train_test_split
from sklearn.metrics import classification_report, accuracy_score
import numpy as np
import itertools
from multiprocessing import Pool


m = 24
k = 11
fs = [0.1, 0.2, 0.3, 0.4]

num_iterations = 100

algorithms = ['l-bfgs', 'sgd', 'adam']
activations = ['logistic', 'relu']
hidden_layer_sizes = [(30,), (50, ), (20, 20), (30, 30), (10, 10, 10), (20, 20, 20), (30, 30, 30)]
learning_rates = ['adaptive']
learning_rate_inits = [0.001, 0.01]

def test_nn(args) :
    print 1
    (f, algorithm, activation, hidden_layer_sizes,\
                 learning_rate, learning_rate_init) = args
    if algorithm == 'l-bfgs' :
        if learning_rate > 0.0005 :
            return (algorithm, activation, hidden_layer_sizes, learning_rate,\
                    learning_rate_init, f, -1.0)
    X = np.load('./data/in_{0}_{1}_{2}.npy'.format(m, k, f))
    y = np.load('./data/target_{0}_{1}_{2}.npy'.format(m, k, f))
    X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                        test_size=0.20,
                                                        random_state=42)
    clf = MLPClassifier(activation =activation, hidden_layer_sizes=hidden_layer_sizes,
                        algorithm = algorithm, learning_rate = learning_rate,
                        learning_rate_init=learning_rate_init, max_iter=1, warm_start=True,
                        random_state=1, shuffle=True)
    for i in range(num_iterations) :
        clf.fit(X_train, y_train)

        y_predict = clf.predict(X_test)

    return (algorithm, activation, hidden_layer_sizes, learning_rate,\
                    learning_rate_init, f, accuracy_score(y_test, y_predict))

def main() :
    params = itertools.product(fs, algorithms, activations, hidden_layer_sizes, learning_rates, learning_rate_inits)
    p = Pool(20)
    results = p.map(test_nn, params)
    with open('nn_results.csv', 'w') as f :
        f.write('algorithm;activation;size;rate;rate_init;f;accuracy\n')
        for r in results :
            f.write('{0};{1};{2};{3};{4};{5};{6}\n'.format(*r))
            

if __name__ == '__main__' :
    main()
