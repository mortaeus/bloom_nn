#functions to apply noise filters to a StochasticBloomFilter

import random

def bsc_noise(msg, f) :
  """adds noise from a binary symmetric channel accoring to flip rate f
     to copy of the provided StochasticBloomFilter"""
  msg = msg.copy()
  for i in range(len(msg._bit_array)) :
    if random.random() < f :
      #flip bit
      msg._bit_array[i] = not msg._bit_array[i]

  return msg
